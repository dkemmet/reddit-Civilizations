#
# VERSION: Milestone 0.1
# REQUIREMENTS: PRAW 5.2, Python 3.4 or greater
# PURPOSE: This init file maps the contents of the doc module so that we can easily refer to them later.
# AUTHORS: RothX, ishouldjust
#

__all__ = ['onreddit', 'crafting', 'trading', 'helptheplayer']