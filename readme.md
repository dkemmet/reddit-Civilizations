# Reddit Civilizations #
Reddit Civilizations is a simulation game. It's like Sid Meyer's Civilizations just reimagined for reddit.

## Features Of The Game ##
* **Questing System** <br />
<tb /><tb />Random Quests are generated and take place on different colonies (subreddits).
* **Currency System** <br />
<tb /><tb /> You can haz money! Trade with it, give it away and horde it. At some point, you'll even get interest!
* **Resource Mining** <br />
<tb /><tb /> Chop, pic, and process your way to domination.
* **Trade System** <br />
<tb /><tb /> Yeah I think this makes sense. 
* **User Migraiton** <br />
<tb /><tb /> Switch colonies at any time. 

## Requirements ##
The following is a comprehensive list of requirements to develop on this bot.
* Python 3.6
* SQLITE3
* PRAW 5.2
* A Debian-based linux environment for execution



## Start Contributing ##
Virtual Environments are highly encouraged, here his how you can create an environment that matches
the exact dev conditions of this bot:<br />
okay now mac the doc.