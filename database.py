#
# VERSION: Milestone 0.1
# REQUIREMENTS: PRAW 5.2, Python 3.4 or greater
# PURPOSE: This module is responsible for translating the Department of Census's actions into actions against the
#          database of the game.
# AUTHORS: RothX, ishouldjust
#

import sqlite3
import os


# responsible for making sure the database and it's required tables exist before the game starts. returns True/False
def init_dbs():
    try:

        # Check for the Database, If it exists and all is well, return true;
        if os.path.lexists('TheColonies.sqlite3') is True:

            # Establish connection and construct cursor
            connection = sqlite3.connect('TheColonies.sqlite3')
            c = connection.cursor()

            # Check for CitizenIndex
            try:
                CitizenIndexResult = c.execute('SELECT * FROM CitizenIndex')
                print("Citizen index found!")
            except:
                print('Colonist index not found in database, creating...')
                CitizenIndex = 'CREATE TABLE `CitizenIndex` (`citizen_name`	TEXT UNIQUE,`colony_assignment`	TEXT,`currency`	INTEGER,`resource_one`	INTEGER,`resource_two`	INTEGER,`resource_three` INTEGER,PRIMARY KEY(`citizen_name`));'
                c.execute(CitizenIndex)
                connection.commit()
                print("Citizen Index Created!")

            # Check for ColonyIndex
            try:
                ColonyIndexResult = c.execute('SELECT * FROM ColonyIndex')
                print('Colony index found!')
            except:
                print('Colony index not found in database, creating...')
                ColonyIndex = 'CREATE TABLE `ColonyIndex` (`colony_name` TEXT, `leader` TEXT UNIQUE,`colony_population`	INTEGER,`total_currency` INTEGER,`resource_list` TEXT);'
                c.execute(ColonyIndex)
            # Close connection to the database, prompt console, return true
            connection.close()
            print("Database check completed successfully!")
            return True

        # If the database isn't found...
        else:

            # Establish connection and construct cursor
            connection = sqlite3.connect('TheColonies.sqlite3')
            c = connection.cursor()

            # Define and execute table queries.
            CitizenIndex =  'CREATE TABLE `CitizenIndex` (`citizen_name`	TEXT UNIQUE,`colony_assignment`	TEXT,`currency`	INTEGER,`resource_one`	INTEGER,`resource_two`	INTEGER,`resource_three` INTEGER,PRIMARY KEY(`citizen_name`));'
            PlayerInventory = ''
            ColonyIndex =  'CREATE TABLE `ColonyIndex` (`colony_name` TEXT, `leader` TEXT UNIQUE,`colony_population`	INTEGER,`total_currency` INTEGER,`resource_list` TEXT);'
            c.execute(CitizenIndex)
            # c.execute(PlayerInvetory)
            c.execute(ColonyIndex)

            # Save work and exit
            connection.commit()
            connection.close()

            return True

    except sqlite3.IntegrityError:
        print("A query did not execute successfully. Passing.")
        pass


#
# CRUD: CitizenIndex
# This table is responsible for tracking players and their associated assets.
#


#
# CRUD: ColonyStatus
# This table is responsible for tracking players and their associated assets.
#